import asyncio
import aiohttp
import logging
import os
import re

from .common import Loader, VideoFile
from urllib.parse import urlparse, urlunparse, parse_qs, urlsplit

logging.basicConfig(level=logging.DEBUG)


class KodikFile(VideoFile):
    def __init__(self, title, quality):
        super(KodikFile, self).__init__()
        self.title = title
        self.season = ''
        self.episode = ''
        self._quality = quality

    @property
    def filename(self):
        return f'{self.title}-season-{self.season}-episode-{self.episode}{self.ext}'

    @property
    def quality(self):
        return self._quality

    @property
    def ext(self):
        # TODO: hardcoded :\
        return '.mp4'


class KodikLoader(Loader):
    """ It is a video downloader from the kodik player. """

    GET_VIDEO_INFO_URL = "https://{host}/get-video-info"
    IFRAME_URL_REGEX = r'iframe.src += +\s*\"(.*?)\";'
    APP_PROMO_URL_REGEX = r'(/assets/js/app.promo.\w*.js)'
    HASH2_VARIABLE_REGEX = r'\s*hash2:\"(\w*)\"'

    async def prepare(self, videofile: VideoFile):
        iframe_url = app_promo_url = hash2 = ''
        params = None
        page_url = self.repair_url(videofile.page_url, scheme='https')
        

        async with self._session.get(page_url) as r:
            html = await r.text()
            p = next(re.finditer(self.IFRAME_URL_REGEX, html), None).group(1)
            iframe_url = self.repair_url(p, scheme='https')
            url_params = parse_qs(urlsplit(page_url).query)
            videofile.season = ''.join(url_params['season'])
            videofile.episode = ''.join(url_params['episode'])


        async with self._session.get(iframe_url) as r:
            html = await r.text()
            p = next(re.finditer(self.APP_PROMO_URL_REGEX, html), None).group(0)
            app_promo_url = self.repair_url(
                p, netloc='kodik.info', scheme='https')

        async with self._session.get(app_promo_url) as r:
            html = await r.text()
            p = next(re.finditer(self.HASH2_VARIABLE_REGEX, html), None).group(1)
            hash2 = p
            params = self.get_params_from_url(iframe_url, hash2)

        async with self._session.post(self.GET_VIDEO_INFO_URL.format(host=params['host']),
                                      data=params['data']) as r:

            video_info = await r.json()
            link = video_info['links'][videofile.quality][0]['src']
            link = re.sub(r':hls:manifest.m3u8', '', link)
            videofile.file_url = self.repair_url(link, scheme='https')

        async with self._session.get(videofile.file_url) as r:
            videofile.size = int(r.headers['Content-Length'])

        videofile.prepared = True if videofile.file_url else None
        return True

    def repair_url(self, url, **kwargs):
        return urlunparse(urlparse(url)._replace(**kwargs))

    def get_params_from_url(self, url, hash2):
        p_url = urlparse(url)
        _host = p_url.netloc
        _, _type, _id, _hash, _quality = list(
            filter(len, p_url.path.split('/')))
        _params = parse_qs(p_url.query)
        _quality = _quality.replace('p', '')
        return {'host': p_url.netloc, 'quality': _quality,
                'data': dict(
                    {'type': _type, 'id': _id,
                     'hash': _hash, 'hash2': hash2,
                        'bad_user': 'true', 'info': '{}'}, **_params)}
